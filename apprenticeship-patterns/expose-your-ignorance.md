
From: http://chimera.labs.oreilly.com/books/1234000001813/ch02.html#expose_your_ignorance

* Action
  * Write down a list of five things you really don’t understand about your work. Put that list where others can see it. Then get in the habit of refreshing this list as your work changes.

  * Steve:
    * 1. I do not know a concrete set of steps to get a product from idea to profitability.
    * 2. I do not know how to validate a philosophy, much less instill it as a foundation of a social graph.
    * 3. I do not know how to multitask or to learn how to multitask. I do not even (really) believe that multitasking is possible.
    * 4. I do not know how to sell designers.
    * 5. I do not know how to build (real) federated systems and sell them profitably.

  * Abhik:
    * 1. I do not have a go-to language that i feel i have expertise in.
    * 2. I feel i do not have enough depth in the things i have worked on.
         I might have accreted too much breadth at the expense of depth.
    * 3. I feel i do not have enough proficiency in the tools i have used.
    * 4. I feel i have worked on the same systems in similar organizational settings for too long.
    * 3. I wish i could touch-type so that my thought-to-code feedback loop could be a lot faster.
